package gmail

// Forked from https://github.com/googleworkspace/go-samples/blob/main/gmail/quickstart/quickstart.go

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"path/filepath"
	"time"

	"github.com/niklasfasching/go-org/org"
	"gitlab.com/josephburnett/org-everything/pkg/format"
	"gitlab.com/josephburnett/org-everything/pkg/input"
	"golang.org/x/oauth2"
	"golang.org/x/oauth2/google"
	"google.golang.org/api/gmail/v1"
	"google.golang.org/api/option"
)

var _ input.I = &gmailInput{}

type gmailInput struct {
	client         *gmail.Service
	starredLabelId string
}

const (
	KeyGmailMessageID        = "GMAIL-MESSAGE-ID"
	KeyGmailMessageFrom      = "GMAIL-MESSAGE-FROM"
	KeyGmailMessageCreatedAt = "GMAIL-MESSAGE-CREATED-AT"
	TagGmail                 = "gmail"
	TagStarred               = "starred"
	SourceGmailMessage       = "gmail-message"
	LabelStarred             = "STARRED"
)

func New() (input.I, error) {
	ctx := context.Background()
	home := os.Getenv("HOME")
	if home == "" {
		return nil, fmt.Errorf("set HOME to find credentials.json")
	}
	// Get `credentials.json` from here: https://console.cloud.google.com/apis/credentials
	b, err := os.ReadFile(home + "/.config/gmail/credentials.json")
	if err != nil {
		log.Fatalf("Unable to read client secret file: %v", err)
	}
	config, err := google.ConfigFromJSON(b, gmail.GmailReadonlyScope, gmail.GmailModifyScope)
	if err != nil {
		log.Fatalf("Unable to parse client secret file to config: %v", err)
	}
	// Uses `token.json` obtained from here: https://github.com/googleworkspace/go-samples/blob/main/gmail/quickstart/quickstart.go
	client := getClient(config)
	srv, err := gmail.NewService(ctx, option.WithHTTPClient(client))
	if err != nil {
		log.Fatalf("Unable to retrieve Gmail client: %v", err)
	}
	g := &gmailInput{
		client: srv,
	}
	labels, err := srv.Users.Labels.List("me").Do()
	if err != nil {
		log.Fatalf("Could not get labels: %v", err)
	}
	for _, l := range labels.Labels {
		if l.Name == LabelStarred {
			g.starredLabelId = l.Id
		}
	}
	if g.starredLabelId == "" {
		return nil, fmt.Errorf("could not find starred label id")
	}
	return g, nil
}

func (g *gmailInput) Reconcile(local, _ []*org.Headline) ([]*org.Headline, error) {
	user := "me"
	remote, err := g.client.Users.Messages.List(user).Q("is:starred").Do()
	if err != nil {
		return nil, err
	}
	remoteMessagesByID := map[string]*gmail.Message{}
	for _, m := range remote.Messages {
		remoteMessagesByID[m.Id] = m
	}
	localHeadlinesByID := map[string]*org.Headline{}
	for _, h := range local {
		source, err := format.GetProperty(h, format.KeyOrgEverythingSource)
		if err != nil {
			continue
		}
		if source != SourceGmailMessage {
			// This is not a Gmail message
			continue
		}
		id, err := format.GetProperty(h, KeyGmailMessageID)
		if err != nil {
			continue
		}
		localHeadlinesByID[id] = h
		m, ok := remoteMessagesByID[id]
		if !ok {
			if h.Status != "DONE" {
				fmt.Fprintf(os.Stderr, "gmail message is no longer starred, marking DONE: %v\n", id)
				h.Status = "DONE"
			}
			continue
		}
		if h.Status == "DONE" {
			fmt.Fprintf(os.Stderr, "unstarring gmail message: %v\n", id)
			err = g.UnstarMessage(m)
			if err != nil {
				return nil, err
			}
		}
	}
	for _, m := range remote.Messages {
		if _, ok := localHeadlinesByID[m.Id]; !ok {
			// New Gmail message
			h, err := g.headlineFromMessage(m)
			if err != nil {
				return nil, err
			}
			fmt.Fprintf(os.Stderr, "new starred gmail message: %v\n", m.Id)
			local = append(local, h)
		}
	}
	return local, nil
}

func (g *gmailInput) UnstarMessage(m *gmail.Message) error {
	_, err := g.client.Users.Messages.Modify("me", m.Id, &gmail.ModifyMessageRequest{
		RemoveLabelIds: []string{g.starredLabelId},
	}).Do()
	return err
}

func (g *gmailInput) headlineFromMessage(m *gmail.Message) (*org.Headline, error) {
	var err error
	// Have to get the actual message with content
	m, err = g.client.Users.Messages.Get("me", m.Id).Do()
	if err != nil {
		return nil, err
	}
	var subject string
	for _, header := range m.Payload.Headers {
		if header.Name == "Subject" {
			subject = header.Value
			break
		}
	}
	h := &org.Headline{
		Lvl:    1,
		Title:  format.Title(subject + ": " + m.Snippet),
		Status: "TODO",
		Properties: &org.PropertyDrawer{
			Properties: [][]string{{
				format.KeyOrgEverythingSource, SourceGmailMessage,
			}, {
				format.KeyOrgEverythingURL, fmt.Sprintf("https://mail.google.com/mail/u/0/#all/%v", m.Id),
			}, {
				KeyGmailMessageID, m.Id,
			}, {
				KeyGmailMessageCreatedAt, time.Unix(0, m.InternalDate*int64(time.Millisecond)).Format(time.RFC822),
			}},
		},
	}
	h.Children = append(h.Children, format.Body(subject+": "+m.Snippet)...)
	h.Tags = append(h.Tags,
		format.TagRemote,
		TagGmail,
		TagStarred,
	)
	return h, nil
}

// Retrieve a token, saves the token, then returns the generated client.
func getClient(config *oauth2.Config) *http.Client {
	// The file token.json stores the user's access and refresh tokens, and is
	// created automatically when the authorization flow completes for the first
	// time.
	tokFile := "token.json"
	tok, err := tokenFromFile(filepath.Join(os.Getenv("HOME"), tokFile))
	if err != nil {
		tok = getTokenFromWeb(config)
		saveToken(tokFile, tok)
	}
	return config.Client(context.Background(), tok)
}

// Request a token from the web, then returns the retrieved token.
func getTokenFromWeb(config *oauth2.Config) *oauth2.Token {
	authURL := config.AuthCodeURL("state-token", oauth2.AccessTypeOffline)
	fmt.Printf("Go to the following link in your browser then type the "+
		"authorization code: \n%v\n", authURL)

	var authCode string
	if _, err := fmt.Scan(&authCode); err != nil {
		log.Fatalf("Unable to read authorization code: %v", err)
	}

	tok, err := config.Exchange(context.TODO(), authCode)
	if err != nil {
		log.Fatalf("Unable to retrieve token from web: %v", err)
	}
	return tok
}

// Retrieves a token from a local file.
func tokenFromFile(file string) (*oauth2.Token, error) {
	f, err := os.Open(file)
	if err != nil {
		return nil, err
	}
	defer f.Close()
	tok := &oauth2.Token{}
	err = json.NewDecoder(f).Decode(tok)
	return tok, err
}

// Saves a token to a file path.
func saveToken(path string, token *oauth2.Token) {
	fmt.Printf("Saving credential file to: %s\n", path)
	f, err := os.OpenFile(path, os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0600)
	if err != nil {
		log.Fatalf("Unable to cache oauth token: %v", err)
	}
	defer f.Close()
	json.NewEncoder(f).Encode(token)
}
