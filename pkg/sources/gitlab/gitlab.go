package gitlab

import (
	"fmt"
	"os"
	"strconv"
	"strings"
	"time"

	"github.com/niklasfasching/go-org/org"
	"github.com/xanzy/go-gitlab"
	gitlabClient "github.com/xanzy/go-gitlab"
	"gitlab.com/josephburnett/org-everything/pkg/format"
	"gitlab.com/josephburnett/org-everything/pkg/input"
)

var _ input.I = &gitlabInput{}

type gitlabInput struct {
	client *gitlabClient.Client
}

const (
	KeyGitlabTodoID                    = "GITLAB-TODO-ID"
	KeyGitlabTodoProject               = "GITLAB-TODO-PROJECT"
	KeyGitlabTodoAuthor                = "GITLAB-TODO-AUTHOR"
	KeyGitlabTodoCreatedAt             = "GITLAB-TODO-CREATED-AT"
	KeyGitlabEngagementGroupID         = "GITLAB-ENGAGEMENT-GROUP-ID"
	KeyGitlabEngagementEpicID          = "GITLAB-ENGAGEMENT-EPIC-ID"
	KeyGitlabEngagementIssueID         = "GITLAB-ENGAGEMENT-ISSUE-ID"
	KeyGitlabEngagementThresholdYellow = "GITLAB-ENGAGEMENT-THRESHOLD-YELLOW"
	KeyGitlabEngagementThresholdRed    = "GITLAB-ENGAGEMENT-THRESHOLD-RED"
	TagGitlab                          = "gitlab"
	SourceGitlabTodo                   = "gitlab-todo"
)

func New() (input.I, error) {
	token := os.Getenv("GL_TOKEN")
	client, err := gitlabClient.NewClient(token)
	if err != nil {
		return nil, err
	}
	return &gitlabInput{
		client: client,
	}, nil
}

func (g *gitlabInput) Reconcile(local, readOnly []*org.Headline) ([]*org.Headline, error) {
	remote := []*gitlab.Todo{}
	page := 1
	for page > 0 {
		r, response, err := g.client.Todos.ListTodos(&gitlabClient.ListTodosOptions{
			ListOptions: gitlabClient.ListOptions{
				Page:    page,
				PerPage: 200,
			},
		})
		if err != nil {
			return nil, err
		}
		remote = append(remote, r...)
		page = response.NextPage
	}
	remoteTodosByID := map[int]*gitlabClient.Todo{}
	for _, t := range remote {
		remoteTodosByID[t.ID] = t
	}
	localHeadlinesByID := map[int]*org.Headline{}
	for _, h := range local {
		source, err := format.GetProperty(h, format.KeyOrgEverythingSource)
		if err != nil {
			continue
		}
		if source != SourceGitlabTodo {
			// This is not a GitLab TODO
			continue
		}
		idString, err := format.GetProperty(h, KeyGitlabTodoID)
		if err != nil {
			continue
		}
		id, err := strconv.Atoi(idString)
		if err != nil || id == 0 {
			return nil, fmt.Errorf("invalid gitlab-todo-id: %v\n", idString)
		}
		localHeadlinesByID[id] = h
		t, ok := remoteTodosByID[id]
		if !ok {
			if h.Status != "DONE" {
				fmt.Fprintf(os.Stderr, "gitlab todo is no longer exists, marking DONE: %v\n", id)
				h.Status = "DONE"
			}
			continue
		}
		if h.Status == "DONE" && t.State == "pending" {
			fmt.Fprintf(os.Stderr, "marking gitlab todo %v as done\n", id)
			_, err := g.client.Todos.MarkTodoAsDone(id)
			if err != nil {
				return nil, err
			}
		}
	}
	for _, t := range remote {
		if _, ok := localHeadlinesByID[t.ID]; !ok {
			// New TODO
			h, err := headlineFromTodo(t)
			if err != nil {
				return nil, err
			}
			fmt.Fprintf(os.Stderr, "new gitlab todo: %v\n", t.ID)
			local = append(local, h)
		}
	}
	for _, i := range []struct {
		headlines []*org.Headline
		fn        func(*org.Headline) error
	}{{
		local, g.checkEngagement,
	}, {
		readOnly, g.checkEngagement,
	}, {
		local, g.checkDeadline,
	}, {
		readOnly, g.checkDeadline,
	}} {
		for _, h := range i.headlines {
			err := i.fn(h)
			if err != nil {
				return nil, err
			}
		}
	}
	return local, nil
}

func headlineFromTodo(t *gitlabClient.Todo) (*org.Headline, error) {
	h := &org.Headline{Lvl: 1}

	switch t.ActionName {
	case gitlabClient.TodoAssigned:
		h.Title = format.Title(t.Target.Title)
	case gitlabClient.TodoMentioned:
		h.Title = format.Title(t.Body)
	case gitlabClient.TodoBuildFailed:
		h.Title = format.Title("The pipeline failed.")
	case gitlabClient.TodoMarked:
		h.Title = format.Title(t.Body)
	case gitlabClient.TodoApprovalRequired:
		h.Title = format.Title("Approval required.")
	case gitlabClient.TodoDirectlyAddressed:
		h.Title = format.Title(t.Body)
	case "review_requested":
		h.Title = format.Title(t.Target.Title)
		h.Tags = append(h.Tags, "URGENT")
	case "member_access_requested":
		h.Title = format.Title(t.Target.Title)
		h.Tags = append(h.Tags, "URGENT")
	case "review_submitted":
		h.Title = format.Title(t.Target.Title)
	case "unmergeable":
		h.Title = format.Title(t.Target.Title)
	case "merge_train_removed":
		h.Title = format.Title(t.Target.Title)
	default:
		return nil, fmt.Errorf("unhandled action name: %v", t.ActionName)
	}

	switch t.State {
	case "pending":
		h.Status = "TODO"
	default:
		return nil, fmt.Errorf("unhandled state: %v", t.State)
	}

	properties := [][]string{{
		format.KeyOrgEverythingSource, SourceGitlabTodo,
	}, {
		format.KeyOrgEverythingURL, t.TargetURL,
	}, {
		KeyGitlabTodoID, strconv.Itoa(t.ID),
	}}
	if t.CreatedAt != nil {
		properties = append(properties, []string{KeyGitlabTodoCreatedAt, t.CreatedAt.Format(time.RFC850)})
	}
	if t.Project != nil {
		properties = append(properties, []string{KeyGitlabTodoProject, t.Project.NameWithNamespace})
	}
	if t.Author != nil {
		properties = append(properties, []string{KeyGitlabTodoAuthor, t.Author.Name})
	}
	h.Properties = &org.PropertyDrawer{
		Properties: properties,
	}

	h.Children = append(h.Children, format.Body(t.Body)...)

	h.Tags = append(h.Tags,
		format.TagRemote,
		TagGitlab,
		string(t.ActionName),
	)

	return h, nil
}

func headlineTitle(content string) []org.Node {
	content = strings.ReplaceAll(content, "\n", " ")
	return []org.Node{org.Text{Content: content, IsRaw: true}}
}

func todoTrimmedBody(todo *gitlabClient.Todo) string {
	body := todo.Body
	body = strings.ReplaceAll(body, "\n", " ")
	if len(body) > 100 {
		body = body[:100]
	}
	return strings.TrimSpace(body) + "\n"
}

func (g *gitlabInput) checkDeadline(item *org.Headline) error {
	if item.Status == "DONE" {
		return nil
	}
	deadline := getDeadline(item.Children)
	if deadline == nil {
		return nil
	}
	now := time.Now()
	daysLeft := int(deadline.Sub(now).Hours() / 24)
	fmt.Fprintf(os.Stderr, "%v days until %v is due\n", daysLeft, item.Title[0])
	return nil
}

func (g *gitlabInput) checkEngagement(item *org.Headline) error {
	if item.Status == "DONE" {
		return nil
	}
	groupID, err := format.GetProperty(item, KeyGitlabEngagementGroupID)
	if groupID == "" || err != nil {
		return nil
	}
	var (
		notes                    []*gitlab.Note
		url                      string
		haveEngagementProperties bool
	)
	if epicID, err := format.GetProperty(item, KeyGitlabEngagementEpicID); epicID != "" && err == nil {
		iid, err := strconv.Atoi(epicID)
		if err != nil {
			return fmt.Errorf("invalid epic id %v: %w", epicID, err)
		}
		epic, _, err := g.client.Epics.GetEpic(groupID, iid)
		if err == nil {
			url = epic.WebURL
		}
		id := epic.ID
		haveEngagementProperties = true
		n, _, err := g.client.Notes.ListEpicNotes(groupID, id, &gitlab.ListEpicNotesOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 200,
			},
		})
		if err != nil {
			return fmt.Errorf("could not get epic notes %v: %w", epicID, err)
		}
		notes = append(notes, n...)
	}
	if issueID, err := format.GetProperty(item, KeyGitlabEngagementIssueID); issueID != "" && err == nil {
		id, err := strconv.Atoi(issueID)
		if err != nil {
			return fmt.Errorf("invalid issue id %v: %w", issueID, err)
		}
		haveEngagementProperties = true
		n, _, err := g.client.Notes.ListIssueNotes(groupID, id, &gitlab.ListIssueNotesOptions{
			ListOptions: gitlab.ListOptions{
				PerPage: 200,
			},
		})
		if err != nil {
			return fmt.Errorf("could not get issue notes %v: %w", issueID, err)
		}
		notes = append(notes, n...)
		issue, _, err := g.client.Issues.GetIssue(groupID, id)
		if err == nil {
			url = issue.WebURL
		}
	}
	if !haveEngagementProperties {
		return nil
	}
	var mostRecentNote time.Time
	for _, n := range notes {
		if n.Author.Username != "josephburnett" {
			continue
		}
		if n.CreatedAt == nil {
			continue
		}
		if n.CreatedAt.After(mostRecentNote) {
			mostRecentNote = *n.CreatedAt
		}
	}
	thresholdRed := 15
	if p, err := format.GetProperty(item, KeyGitlabEngagementThresholdRed); err == nil && p != "" {
		thresholdRed, err = strconv.Atoi(p)
		if err != nil {
			return fmt.Errorf("reading red threshold %v: %w", p, err)
		}
	}
	var daysSinceLastUpdate int = int(time.Now().Sub(mostRecentNote).Hours() / float64(24))
	thresholdYellow := 8
	if p, err := format.GetProperty(item, KeyGitlabEngagementThresholdYellow); err == nil && p != "" {
		thresholdYellow, err = strconv.Atoi(p)
		if err != nil {
			return fmt.Errorf("reading yellow threshold %v: %w", p, err)
		}
	}

	fmt.Fprintf(os.Stderr, "[")
	fmt.Fprint(os.Stderr, colorYellow)
	fmt.Fprintf(os.Stderr, "%v ", thresholdYellow)
	fmt.Fprint(os.Stderr, colorRed)
	fmt.Fprintf(os.Stderr, "%v", thresholdRed)
	fmt.Fprint(os.Stderr, colorDefault)
	fmt.Fprintf(os.Stderr, "] ")
	switch {
	case daysSinceLastUpdate > 100000:
		fmt.Fprintf(os.Stderr, "never updated %q: %v\n", item.Title[0], url)
	case daysSinceLastUpdate > thresholdRed:
		fmt.Fprint(os.Stderr, colorRed)
		fmt.Fprintf(os.Stderr, "%v days since update on %q: %v\n", daysSinceLastUpdate, item.Title[0], url)
		fmt.Fprint(os.Stderr, colorDefault)
	case daysSinceLastUpdate > thresholdYellow:
		fmt.Fprint(os.Stderr, colorYellow)
		fmt.Fprintf(os.Stderr, "%v days since update on %q: %v\n", daysSinceLastUpdate, item.Title[0], url)
		fmt.Fprint(os.Stderr, colorDefault)
	default:
		fmt.Fprint(os.Stderr, colorGreen)
		fmt.Fprintf(os.Stderr, "%v days since update on %q: %v\n", daysSinceLastUpdate, item.Title[0], url)
		fmt.Fprint(os.Stderr, colorDefault)
	}
	return nil
}

func getDeadline(children []org.Node) *time.Time {
	if len(children) == 0 {
		return nil
	}
	switch node := children[0].(type) {
	case org.Paragraph:
		return getDeadline(node.Children)
	case org.Text:
		if strings.Contains(node.Content, "DEADLINE") && len(children) > 1 {
			deadline, ok := children[1].(org.Timestamp)
			if !ok {
				return nil
			}
			return &deadline.Time
		} else {
			return nil
		}
	default:
		return nil
	}
}

const (
	colorDefault = "\033[0m"
	colorRed     = "\033[31m"
	colorGreen   = "\033[32m"
	colorYellow  = "\033[33m"
)
