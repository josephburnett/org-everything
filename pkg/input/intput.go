package input

import "github.com/niklasfasching/go-org/org"

type I interface {
	Reconcile(local, readOnly []*org.Headline) (new []*org.Headline, err error)
}
