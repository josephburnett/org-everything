package format

import (
	"fmt"
	"strings"

	"github.com/niklasfasching/go-org/org"
)

const (
	KeyOrgEverythingSource = "ORG-EVERYTHING-SOURCE"
	KeyOrgEverythingURL    = "ORG-EVERYTHING-URL"
	TagRemote              = "REMOTE"
)

func Title(title string) []org.Node {
	title = strings.ReplaceAll(title, "\n", " ")
	if len(title) > 60 {
		title = title[:60]
	}
	title = strings.TrimSpace(title)
	return []org.Node{org.Text{Content: title, IsRaw: true}}
}

func Body(body string) []org.Node {
	var b strings.Builder
	currentLine := 0
	currentColumn := 0
	for _, word := range strings.Fields(body) {
		fmt.Fprintf(&b, "%s ", word)
		currentColumn += len(word) + 1
		if currentColumn > 80 {
			currentLine += 1
			if currentLine == 10 {
				break
			}
			b.WriteRune('\n')
			currentColumn = 0
		}
	}
	return []org.Node{org.Text{Content: "\n" + b.String() + "\n\n", IsRaw: true}}
}

func GetProperty(h *org.Headline, key string) (string, error) {
	properties := h.Properties
	if h.Properties == nil {
		// Timestamps move the drawer to be a child
		for _, c := range h.Children {
			if p, ok := c.(org.PropertyDrawer); ok {
				properties = &p
			}
		}
		if properties == nil {
			maybeTitle := "(title unknown)"
			if h.Title != nil && len(h.Title) >= 1 {
				maybeTitle = h.Title[0].String()
			}
			return "", fmt.Errorf("cannot find property drawer: %v", maybeTitle)
		}
	}
	var value string
	for _, p := range properties.Properties {
		if len(p) != 2 {
			continue
		}
		if p[0] == key {
			value = p[1]
		}
	}
	return value, nil
}
