package main

import (
	"fmt"
	"os"

	"github.com/niklasfasching/go-org/org"
	"gitlab.com/josephburnett/org-everything/pkg/sources/gitlab"
	"gitlab.com/josephburnett/org-everything/pkg/sources/gmail"
)

func main() {
	infile := os.Getenv("INFILE")
	if infile == "" {
		panic("set INFILE")
	}
	logfile := os.Getenv("LOGFILE")
	if logfile == "" {
		panic("set LOGFILE")
	}
	outfile := os.Getenv("OUTFILE")
	if outfile == "" {
		panic("set OUTFILE")
	}

	d, err := readFile(infile)
	if err != nil {
		panic(err)
	}
	headlines, err := getHeadlines(d, false)
	if err != nil {
		panic(err)
	}
	readOnlyD, err := readFile(logfile)
	if err != nil {
		panic(err)
	}
	readOnlyHeadlines, err := getHeadlines(readOnlyD, true)
	if err != nil {
		panic(err)
	}

	gl, err := gitlab.New()
	if err != nil {
		panic(err)
	}
	headlines, err = gl.Reconcile(headlines, readOnlyHeadlines)
	if err != nil {
		panic(err)
	}

	gm, err := gmail.New()
	if err != nil {
		panic(err)
	}
	headlines, err = gm.Reconcile(headlines, readOnlyHeadlines)
	if err != nil {
		panic(err)
	}

	d.Nodes = nil
	for i, h := range headlines {
		h.Index = i
		d.Nodes = append(d.Nodes, *h)
	}

	w := org.NewOrgWriter()
	outString, err := d.Write(w)
	if err != nil {
		panic(err)
	}
	os.WriteFile(outfile, []byte(outString), 0644)
}

func readFile(filename string) (*org.Document, error) {
	in, err := os.Open(filename)
	if err != nil {
		return nil, err
	}
	defer in.Close()
	d := org.New().Parse(in, filename)
	if err != nil {
		return nil, err
	}
	return d, nil
}

func getHeadlines(d *org.Document, readOnly bool) ([]*org.Headline, error) {
	headlines := []*org.Headline{}
	nodes := d.Nodes
	for _, n := range nodes {
		h, ok := n.(org.Headline)
		if !ok && readOnly {
			continue
		}
		if !ok {
			return nil, fmt.Errorf("unsupported top-level node type: %T", n)
		}
		headlines = append(headlines, &h)
	}
	return headlines, nil

}
